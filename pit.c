#include "pit.h"

void initialize_pit(){

	//__enable_irq();
	SIM -> SCGC6 |= SIM_SCGC6_PIT_MASK;
	// short PIT
	PIT -> CHANNEL[0].LDVAL = 15000000;
	PIT -> CHANNEL[0].TCTRL = PIT_TCTRL_TIE_MASK;
	// long PIT
	PIT -> CHANNEL[1].LDVAL = 40000000;
	PIT -> CHANNEL[1].TCTRL = PIT_TCTRL_TIE_MASK;
	
	PIT -> MCR = 0x00;
	
	NVIC_ClearPendingIRQ(PIT_IRQn); 	/* Clear any pending interrupt */
	NVIC_EnableIRQ(PIT_IRQn);				/* Notify Nested Vector Interrupt Controller */
	NVIC_SetPriority (PIT_IRQn, 128);
}

void reset_pit(void){
	PIT -> CHANNEL[0].TCTRL &= ~(PIT_TCTRL_TEN_MASK);
	PIT -> CHANNEL[0].TCTRL |= PIT_TCTRL_TEN_MASK;
}

void disable_short_pit(void){
	PIT -> CHANNEL[0].TCTRL &= ~(PIT_TCTRL_TEN_MASK);
}

void enable_short_pit(void){
	PIT -> CHANNEL[0].TCTRL |= PIT_TCTRL_TEN_MASK;
}

void disable_long_pit(void){
	PIT -> CHANNEL[1].TCTRL &= ~(PIT_TCTRL_TEN_MASK);
}

void enable_long_pit(void){
	PIT -> CHANNEL[1].TCTRL |= PIT_TCTRL_TEN_MASK;
}