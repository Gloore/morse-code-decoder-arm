#include "MKL46Z4.h"                    
#include "buttons.h"
#include "leds.h"
#include <stdbool.h>

#define SW1_PIN 3												
#define SW3_PIN 12

#define PORTC_D_IRQ_NBR (PORTC_PORTD_IRQn)

void buttonsInitialize(void){
	
	SIM->SCGC5 |=  SIM_SCGC5_PORTC_MASK; 					
	PORTC->PCR[SW1_PIN] |= PORT_PCR_MUX(1);      	
	
	PORTC->PCR[SW1_PIN] |=  PORT_PCR_PE_MASK;		
	PORTC->PCR[SW1_PIN] |=	PORT_PCR_PS_MASK;		
	PORTC->PCR[SW1_PIN] |= 	PORT_PCR_IRQC(0x0a);		
		
	NVIC_ClearPendingIRQ(PORTC_D_IRQ_NBR);				
	NVIC_EnableIRQ(PORTC_D_IRQ_NBR);							
	
	NVIC_SetPriority (PORTC_D_IRQ_NBR, 64);			 

}

int32_t buttonRead(){
	return FPTC->PDIR & (1UL<<SW1_PIN);						/* Get port data input register (PDIR) */
}	

